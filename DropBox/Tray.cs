﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Principal;
using System.Windows.Forms;
using Coop.DropBox.Commands;
using Coop.DropBox.Exceptions;
using Coop.DropBox.Properties;

namespace Coop.DropBox
{
    /// <summary>
    /// Tray is the form that holds the ScanningTimer and the ContextMenu
    /// </summary>
    public partial class Tray : Form
    {
        private readonly Queue<ICommand> commands;
        private readonly string pattern;
        private int fileIndex = 1;
        private bool processing = false;
        
        public Tray()
        {
            InitializeComponent();
            commands = new Queue<ICommand>();
            
            var identity = WindowsIdentity.GetCurrent();
            if (identity != null)
            {
                var parts = identity.Name.Split('\\');
                var name = parts[parts.Length-1];

                if(name.Length > 8) { name = name.Substring(0, 8); } //Only used because ViC has a strict 8 character limit on user names

                pattern = String.Format("{0}.*", name);
                // testBP pattern  = "*.*" ;
            }
            else
            {
                pattern = "*.*";
            }
        }

        private void RequestTimer_Tick(object sender, EventArgs e)
        {
            string[] files;
            RequestTimer.Enabled = false;

            try
            {
                files = Directory.GetFiles(Settings.Default.RequestFolderPath, pattern, SearchOption.TopDirectoryOnly);
            }
            catch
            {
                var btn = MessageBox.Show(
                    "Unable to access " + Settings.Default.RequestFolderPath,
                    "Path Inaccessible",
                    MessageBoxButtons.RetryCancel
                    );

                if (btn == DialogResult.Cancel) { Close(); }
                else                            { RequestTimer.Enabled = true; }
                return; // exit no matter what the choise
            }

            foreach (var file in files)
            {
                var ext = Path.GetExtension(file);
                switch (ext)
                {
                    case ".drop":
                        commands.Enqueue(new DropCommand(this, HiddenRequest(file)));
                        break;
                    case ".scan":
                        commands.Enqueue(new ScanCommand(HiddenRequest(file)));
                        break;
                    case ".view":
                        commands.Enqueue(new ViewCommand(HiddenRequest(file)));
                        break;
                    case ".app":
                        commands.Enqueue(new AppendCommand(HiddenRequest(file)));
                        break;
                }
            }

            ProcessCommands();

            RequestTimer.Enabled = true;
        }

        private void SettingsMenu_Click(object sender, EventArgs e)
        {
            var f = new ApplicationSettings();
            f.ShowDialog(this);
        }

        private void ExitMenu_Click(object sender, EventArgs e)
        {
            Close();
        }

        private string HiddenRequest(string original)
        {
            var temp = Path.GetTempFileName();
            File.Delete(temp);
            File.Move(original, temp);
            return temp;
        }

        private void ProcessCommands()
        {
            if (processing) return;

            processing = true;

            while (commands.Count > 0)
            {
                var command = commands.Peek();

                try
                {
                    command.Execute();
                    commands.Dequeue(); // take it out of the queue only if it executed with no errors
                }
                catch (FileNotFoundException)
                { }
                catch (ScannerNotConfiguredException)
                {
                    var f = new ApplicationSettings();
                    f.ShowDialog(this);
                }
                catch (NoPagesScannedException)
                {
                    var button =
                        MessageBox.Show(
                            "Please put the pages to be scanned in the scanner, and click Retry when you are ready.",
                            "No Pages Found", MessageBoxButtons.RetryCancel);
                    if (button == DialogResult.Cancel) commands.Dequeue();
                    
                }
                catch (Exception ex)
                {
                    var button = MessageBox.Show(ex.Message,"Error",MessageBoxButtons.RetryCancel);
                    if (button == DialogResult.Cancel) commands.Dequeue();
                }
            }

            fileIndex = 0;
            processing = false;
        }

    }
}