﻿using System;
using System.Text;
using System.Windows.Forms;
using Coop.DropBox.Properties;
using WIA;
using CommonDialog = WIA.CommonDialog;

namespace Coop.DropBox
{
    public partial class ApplicationSettings : Form
    {
        private string deviceID;

        public ApplicationSettings()
        {
            InitializeComponent();
            ReadSettings();
        }

        private void BrowseDevice_Click(object sender, EventArgs e)
        {

            int xyz = 0; // test breakpoint 
            try
            {
                WIA.CommonDialog dialog = new WIA.CommonDialogClass();
                WIA.Device device = dialog.ShowSelectDevice(WiaDeviceType.UnspecifiedDeviceType, true, false);
                if (device == null) return;
                DeviceName.Text = (string)device.Properties["Name"].get_Value();

                deviceID = device.DeviceID;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Please check that the scanner is plugged into the computer.", "Missing Device", MessageBoxButtons.OK);
            }
            
        }

        private void ReadSettings()
        {
            RequestFolderPath.Text = Settings.Default.RequestFolderPath;
            BaseSavePath.Text = Settings.Default.BaseSavePath;
            DeviceName.Text = Settings.Default.DeviceName;
            DPI_field.Value = Settings.Default.SelectedDPI;
            ImageType_Field.Text = Settings.Default.ImageType;
            //DeviceName.Text = "EPSON GT-S50";
        }

        private void BrowseForRequestFolder_Click(object sender, EventArgs e)
        {
            FindFolder.SelectedPath = RequestFolderPath.Text;
            FindFolder.ShowDialog();
            RequestFolderPath.Text = FindFolder.SelectedPath;
        }

        private void BrowseForBaseSave_Click(object sender, EventArgs e)
        {
            FindFolder.SelectedPath = BaseSavePath.Text;
            FindFolder.ShowDialog();
            BaseSavePath.Text = FindFolder.SelectedPath;
        }

        private void Save()
        {
            Settings.Default.RequestFolderPath = RequestFolderPath.Text;
            Settings.Default.BaseSavePath = BaseSavePath.Text;
            Settings.Default.DeviceName = DeviceName.Text;
            Settings.Default.DeviceID = (deviceID == null) ? Settings.Default.DeviceID : deviceID;
            Settings.Default.SelectedDPI = (int)DPI_field.Value;
            Settings.Default.ImageType = ImageType_Field.SelectedItem.ToString();

            Settings.Default.Save();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Apply_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void Ok_Click(object sender, EventArgs e)
        {
            Save();
            Close();
        }
    }
}
