﻿using System.IO;
using Coop.DropBox.Properties;

namespace Coop.DropBox
{
    class DropRequest
    {
        // holds an open reference to a file that was found
        private string requestFile;

        /// <summary>
        /// Deletes the request file to indicate the request has been completed
        /// </summary>
        public void Complete()
        {
            if (!Found) return;
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers(); 

            File.Delete(requestFile);

            requestFile = null;
            SaveTo = null;
        }

        /// <summary>
        /// Has a request file been found?
        /// </summary>
        private bool Found
        {
            get { return File.Exists(requestFile); }
        }

        /// <summary>
        /// Where should the files be saved to?
        /// </summary>
        public string SaveTo { get; private set; }

        /// <summary>
        /// Attempt to find the next request file
        /// </summary>
        /// <returns>true if one was found, else false</returns>
        public bool Find()
        {
            // CHECK: if the last found file still exists, do not try to find next
            if (Found)
            {
                return true;
            }

            // look for a new file
            var dir = new DirectoryInfo(Settings.Default.RequestFolderPath);
            var files = dir.GetFiles("*.drop", SearchOption.TopDirectoryOnly);
            if (files.Length > 0)
            {
                // return the first file found
                requestFile = files[0].FullName;
                var folder = "" + files[0].OpenText().ReadLine();
                SaveTo = Path.Combine(Settings.Default.BaseSavePath, folder);
            }

// ReSharper disable RedundantAssignment
            files = null; // try and de-reference the files
            dir = null;
// ReSharper restore RedundantAssignment

            return Found;
        }
    }    
}
