﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Coop.DropBox.Services
{
    interface IScannerService
    {
        Bitmap ScanPage();
    }
}
