﻿using System.Drawing;
using System.Linq;
using Coop.DropBox.Properties;
using WIA;

namespace Coop.DropBox.Services
{
    class ScannerService : IScannerService
    {
        private readonly WIA.Device scanner;

        public ScannerService()
        {
            DeviceManager manager = new DeviceManagerClass();
            var info = manager.DeviceInfos
                .Cast<DeviceInfo>()
                .Where(i => i.DeviceID == Settings.Default.DeviceID)
                .FirstOrDefault();
            if (info == null) return;
            scanner = info.Connect();
        }

        public Bitmap ScanPage()
        {
            Item item = scanner.ExecuteCommand(CommandID.wiaCommandChangeDocument);
            return null;
        }

    }
}
