﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Coop.DropBox.DroppedFiles;
using Coop.DropBox.Properties;
using Coop.DropBox.util;

namespace Coop.DropBox
{

    public partial class Box : Form
    {
        private readonly List<FileInfo> droppedFiles = new List<FileInfo>();
        private readonly List<FileInfo> tempFiles = new List<FileInfo>();
        public string SaveTo;

        public Box()
        {
            InitializeComponent();
        }

        private void Box_Close() {
            foreach (var file in tempFiles)
            {
                try
                {
                    file.Delete(); // delete the temp file we created when the outlook file was dragged to the drop box
                }
                catch (Exception)
                {}
            }

            tempFiles.Clear();
            droppedFiles.Clear();
            
            Close();
        }

        private void Save_Click(object sender, EventArgs e)
        {
            var driveName = SaveTo.Substring(0, 3);
            var msg = "Not able to access drive '" + driveName + "'.";

            // is the drive accessible?
            while (!DriveInfo.GetDrives().Where(d => d.Name == driveName).Any())
            {
                var answer = MessageBox.Show(this, msg, "Missing Drive", MessageBoxButtons.RetryCancel);
                if (answer == DialogResult.Cancel) return;
            }
            
            // create the destination folder
            if (!Directory.Exists(SaveTo)) {
                while (true) {
                    try { 
                        Directory.CreateDirectory(SaveTo);
                        break;
                    }
                    catch (Exception ex) {
                        var answer = MessageBox.Show(this, ex.Message, "Create Directory", MessageBoxButtons.RetryCancel);
                        if (answer == DialogResult.Cancel) return;
                    }
                }
            }

            foreach (var file in droppedFiles) {
                file.CopyTo(Path.Combine(
                    SaveTo,
                    file.Name
                    ),true);
            }

            Box_Close();
        }
        
        private void Cancel_Click(object sender, EventArgs e)
        {
            Box_Close();
        }

        private void BoxList_DragEnter(object sender, DragEventArgs e)
        {
            // make sure they're actually dropping descriptorData (not text or anything else)
            if (e.Data.GetDataPresent("FileDrop") || e.Data.GetDataPresent("FileGroupDescriptor"))
            { e.Effect = DragDropEffects.All; }
            else          
            { e.Effect = DragDropEffects.None; }
        }

        private void BoxList_DragDrop(object sender, DragEventArgs e)
        {
            List<FileInfo> dropped;
            
            // 1. Get the correct dropped files handler
            if (e.Data.GetDataPresent("FileGroupDescriptor"))
            {
                dropped = (new OutlookDroppedFiles()).GetFiles(e);
                foreach (var file in dropped.Where(file => !FileComparer.Contains(tempFiles, file)))
                {
                    tempFiles.Add(file); // track outlook files so they get deleted later
                }
            }
            else {
                dropped = (new FileSystemDroppedFiles()).GetFiles(e);
            }

            // 2. Loop through the string array, adding each filename to the ListBox
            foreach (var file in dropped.Where(file => !FileComparer.Contains(droppedFiles, file)))
            {
                droppedFiles.Add(file);
    
                // check if the ImageList already has an image for this File Extention
                if (ListViewImages.Images.ContainsKey(file.Extension) == false)
                {
                    // add missing icon
                    var fileIcon = FileIconExtractor.GetIcon(file.FullName, false);
                    ListViewImages.Images.Add(file.Extension, fileIcon);
                }


                // add a new ListViewItem to the ListView for this file
                BoxList.Items.Add(new ListViewItem(
                                      file.Name, 
                                      ListViewImages.Images.IndexOfKey(file.Extension)
                                      ));
            }
        }

        private void RemoveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in BoxList.SelectedItems)
            {
                // if the item was from outlook, delete it
                foreach (var file in tempFiles)
                {
                    if (file.Name == item.Name)
                    {
                        tempFiles.Remove(file);
                        file.Delete();
                    }
                }

                // remove the item from the list of dropped Files 
                foreach (var file in droppedFiles)
                {
                    if (file.Name == item.Name)
                    {
                        droppedFiles.Remove(file);
                    }
                }

                // remove from ListView
                item.Remove();
            }
        }

        private void Box_Shown(object sender, EventArgs e)
        {
            Location = new Point(
                    SystemInformation.WorkingArea.Width - Width,
                    SystemInformation.WorkingArea.Height - Height
                    );
        }
    }
}



