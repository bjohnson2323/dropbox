﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Coop.DropBox.Commands
{
    interface ICommand
    {
        void Execute();
    }
}
