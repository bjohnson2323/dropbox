﻿using System.Diagnostics;
using System.IO;
using Coop.DropBox.Properties;

namespace Coop.DropBox.Commands
{
    class ViewCommand : ICommand
    {
        private readonly string file;

        public ViewCommand( string fileName)
        {
            file = fileName;
        }

        /// <summary>
        /// Tries to open the file with the application bound to the Open verb
        /// </summary>
        /// <exception cref="FileNotFoundException">Thrown when the path supplied to the command's constructor is not able to be found</exception>
        /// <exception cref="System.ComponentModel.Win32Exception">Thrown when the Open verb is not bound to an application for the file type</exception>
        public void Execute()
        {
            // open the request file and read out the first line
            if (!File.Exists(file)) throw new FileNotFoundException("Could not find the view request file", file);
            var lines = File.ReadAllLines(file);
            var folderName = lines[0];
            var fileName = lines[1]; // the first line should be the path to save to
            var path = Path.Combine(Settings.Default.BaseSavePath, folderName, fileName);

            // launch the application associated with the file's Open verb 
            var startInfo = new ProcessStartInfo(path) {Verb = "Open"};
            var process = new Process {StartInfo = startInfo};
            process.Start();

            // remove the request file now that we are done
            File.Delete(file);
        }
    }
}
