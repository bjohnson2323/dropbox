﻿using System.IO;
using System.Windows.Forms;
using Coop.DropBox.Properties;

namespace Coop.DropBox.Commands
{
    class DropCommand : ICommand
    {
        private readonly IWin32Window owner;
        private readonly string file;

        public DropCommand(IWin32Window parent, string fileName)
        {
            owner = parent;
            file = fileName;
        }

        public void Execute()
        {
            // open the request file and read out the first line
            if (!File.Exists(file)) throw new FileNotFoundException("Could not find the drop request file", file);
            var lines = File.ReadAllLines(file);
            var saveTo = Path.Combine(Settings.Default.BaseSavePath,lines[0]); // the first line should be the path to save to
            
            // use the Box dialog for user interaction to drag-n-drop files
            var f = new Box { SaveTo = saveTo };
            f.ShowDialog(owner);

            // delete the request file now that we are done processing the request
            File.Delete(file);
        }
    }
}
