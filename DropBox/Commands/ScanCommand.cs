﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Coop.DropBox.Exceptions;
using Coop.DropBox.Properties;
using Coop.DropBox.util;
using PdfSharp.Pdf;
using WIA;
using PdfSharp;
using PdfSharp.Drawing;

namespace Coop.DropBox.Commands
{
    class ScanCommand : ICommand
    {
        private readonly string file;

        private enum DocumentHandelingStatus : int
        {
            FeedReady = 0x01, 
            FlatReady = 0x02, 
            DupReady = 0x04, 
            FlatCoverUp = 0x08, 
            PathCoverUp = 0x10, 
            PaperJam = 0x20, 
        }

        public ScanCommand( string fileName )
        {
            file = fileName;
        }

        public void Execute()
        {
            // 1. get the contents of the request file
            if (!File.Exists(file)) throw new FileNotFoundException("Could not find the action request file", file);
            var lines = File.ReadAllLines(file);
            
            // Does the save path exist?
            if (!Directory.Exists(Settings.Default.BaseSavePath)) throw new DirectoryNotFoundException("Could not find the configured save folder.");
            var saveFolder = Path.Combine(Settings.Default.BaseSavePath,lines[0]); // sub-folder of the base directory
            if (!Directory.Exists(saveFolder)) throw new DirectoryNotFoundException("Could not find the specified sub-folder '" + lines[0] + "'.");
            var fileName = lines[1];
            var saveAs = Path.Combine(saveFolder, fileName + ".pdf"); // file name without extention

            // 2. conenct and configure the scanner
            var scanner = GetDevice();
            if (scanner == null) throw new ScannerNotConfiguredException("Default scanner not accessible, please configure a scanner");

            // 3. scan all of the pages in the scanner
            var pages = new List<ImageFile>();
            var item = scanner.Items[1];
            var temp = Path.Combine(Path.GetTempPath(), fileName);
            Directory.CreateDirectory(temp);
            var n = 0; // page number
            
            while (true)
            {
                try
                {
                    // scan until there is an error...
                    ImageFile page = null;
                    page = (ImageFile)item.Transfer(FormatID.wiaFormatTIFF);
                    n += 1; // increment the page number
                    page.SaveFile(
                        Path.Combine(
                            temp, // temp folder for this command 
                            n.ToString().PadLeft(3, '0') + ".tiff"));
                }
                catch (Exception ex)
                {
                    if (n == 0) throw new NoPagesScannedException();
                    break;
                }
            }

            // 4. convert all .tiff files in the temp folder to a single PDF
            MergeToPDF(temp, saveAs);

            // 5. clean up temp folder
            Directory.Delete(temp, true);
            
            // 6. delete the request file
            File.Delete(file);
        }

        private IDevice GetDevice()
        {
            //  get a collection of all device info objects
            var devices = new DeviceManagerClass().DeviceInfos.Cast<DeviceInfo>();

            // get the device info for the device with the same ID as the one saved in settings
            var info = devices
                .Where(device => device.DeviceID == Settings.Default.DeviceID)
                .FirstOrDefault();

            // if not found, just exit
            if (info == null) return null;

            // if found, configure it for scanning
            var scanner = info.Connect();
            Configure(scanner);

            // return the connected and configured scanner
            return scanner;
        }

        private void Configure(IDevice device)
        {
            int maxDPIValue = device.Items[1].Properties["6147"].SubTypeMax;
            int minDPIValue = device.Items[1].Properties["6147"].SubTypeMin;

            //determine if inputted DPI falls outside range of scanner and set to the min or max accordingly
            int dpi = (Settings.Default.SelectedDPI > maxDPIValue) ? maxDPIValue : Settings.Default.SelectedDPI;
            dpi = (dpi < minDPIValue) ? minDPIValue : dpi;
            Settings.Default.SelectedDPI = dpi;

            var imageIntent = (Settings.Default.ImageType == "Color") ?  WiaImageIntent.ColorIntent : WiaImageIntent.GrayscaleIntent;

            //List<string> test = new List<string>(); // keeping this as a way to iterate through options for future dev work
            //foreach(IProperty prop in device.Items[1].Properties)
            //{ test.Add(prop.PropertyID.ToString() + ": " + prop.Name.ToString() + " - " + prop.get_Value().ToString()); }

            //keep for reference for combinding different intents
            //device.Items[1].Properties["Current Intent"].set_Value(0x10002); // Color intent == grey scale + minimize

            device.Properties["3088"].set_Value(0x001); // FEEDER & NEXT_PAGE
            device.Properties["3096"].set_Value(1);     // No. of pages set to 1 so that each page is aquired as a new transfer
            
            device.Items[1].Properties["Current Intent"].set_Value(imageIntent); // Color intent == grey scale + minimize

            device.Items[1].Properties["6147"].set_Value(dpi); // DPI horizontal
            device.Items[1].Properties["6148"].set_Value(dpi); // DPI vertical

            device.Items[1].Properties["6149"].set_Value(0); // x point to start scan
            device.Items[1].Properties["6150"].set_Value(0); // y point to start scan

            device.Items[1].Properties["6151"].set_Value( 8.5 * dpi); // Horizontal extent
            device.Items[1].Properties["6152"].set_Value(11.0 * dpi); // Vertical extent
        }

        private bool FeedReady(IDevice device)
        {
            var status = (DocumentHandelingStatus)device.Properties["Document Handling Status"].get_Value();
            return status == DocumentHandelingStatus.FeedReady;
        }

        private void MergeToPDF(string folder, string saveAs)
        {
            if (!Directory.Exists(folder)) return;

            var tiff = new TiffImageSplitter();
            var pdf = new PdfDocument();

            // pdf.Options.ColorMode = PdfColorMode.Cmyk;
            pdf.Options.CompressContentStreams = true;
            pdf.Version = 16; // version 1.6 == Acrobat 7
            
            var files = Directory.GetFiles(folder).Where(f => f.EndsWith(".tiff")).ToList();
            var n = 0;
            foreach (var tiffFile in files.OrderBy(f => f))
            {
                var path = Path.Combine(folder, tiffFile);
                var count = tiff.getPageCount(path);
                for (var i = 0; i < count; i++)
                {
                    var raw = tiff.getTiffImage(path, i);
                    //raw = tiff.AdjustContrast(raw, 2.0f); // double the contrast
                    var image = XImage.FromGdiPlusImage(raw);
                    
                    // TODO: if image is a blank page, continue
                    
                    var page = new PdfPage {Height = image.PointHeight , Width = image.PointWidth};
                    pdf.Pages.Add(page);

                    var graphics = XGraphics.FromPdfPage(pdf.Pages[n]);
                    graphics.DrawImage(image, 0, 0);
                    graphics.Dispose();

                    n += 1; // next page
                }

                // remove the temp file
                File.Delete(path);
            }

            pdf.Save(saveAs);
            pdf.Close();
        }
    }
}
