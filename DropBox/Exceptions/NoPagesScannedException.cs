﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Coop.DropBox.Exceptions
{
    class NoPagesScannedException : ApplicationException
    {
        public NoPagesScannedException()
            : base()
        {
        }
        public NoPagesScannedException(string message)
            : base(message)
        {
        }
        //public NoPagesScannedException(SerializtionInfo info, SreamingContext context)
        //    : base(info, context)
        //{
        //}
        public NoPagesScannedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
