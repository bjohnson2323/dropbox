﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Coop.DropBox.Exceptions
{
    class ScannerNotConfiguredException : ApplicationException
    {
        public ScannerNotConfiguredException() 
            : base ()
        {
        }
        public ScannerNotConfiguredException(string message)
            : base(message)
        {
        }
        //public ScannerNotConfiguredException(SerializtionInfo info, SreamingContext context)
        //    : base(info, context)
        //{
        //}
        public ScannerNotConfiguredException(string message, Exception innerException)
            : base(message,innerException)
        {
        }
    }
}
