﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Coop.DropBox.DroppedFiles
{
    class FileSystemDroppedFiles : IDroppedFiles
    {
        public List<FileInfo> GetFiles(DragEventArgs e)
        {
            // 1. Get the file names
            var names = (string[])e.Data.GetData(DataFormats.FileDrop);

            // 2. Loop through the array of names and add FileInfo objects
            return names.Select(name => new FileInfo(name)).ToList();
        }
    }
}
