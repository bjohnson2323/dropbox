﻿using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Coop.DropBox.util;

namespace Coop.DropBox.DroppedFiles
{
    class OutlookDroppedFiles : IDroppedFiles
    {
        public List<FileInfo> GetFiles(DragEventArgs e)
        {
            var files = new List<FileInfo>();

            // 1. Wrap standard IDataObject in OutlookDataObject
            var outlook = new OutlookDataObject(e.Data);

            // 2. Get the names and data streams of the files dropped
            var names = (string[])outlook.GetData("FileGroupDescriptorW");
            var streams = (MemoryStream[])outlook.GetData("FileContents");

            // 3. Save the files to a temp folder
            var folder = Path.GetTempPath();

            for (var i = 0; i < names.Length; i++)
            {
                // a. Use the file index to get the name and data stream
                var name = names[i];
                var stream = streams[i];
                
                // b. Save the file stream using its name to the application path
                var path = Path.Combine( folder, name );
                var file = File.Create( path );
                stream.WriteTo( file );
                file.Close();

                // c. Append the file to the output array
                files.Add( new FileInfo(path) );
            }
            
            // 4. Returm the list of FileInfo objects
            return files;
        }
    }
}