﻿namespace Coop.DropBox
{
    partial class Box
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Cancel = new System.Windows.Forms.Button();
            this.Save = new System.Windows.Forms.Button();
            this.BoxList = new System.Windows.Forms.ListView();
            this.ListContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ListViewImages = new System.Windows.Forms.ImageList(this.components);
            this.ListContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // Cancel
            // 
            this.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel.Location = new System.Drawing.Point(183, 159);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(75, 23);
            this.Cancel.TabIndex = 0;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // Save
            // 
            this.Save.Location = new System.Drawing.Point(102, 159);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(75, 23);
            this.Save.TabIndex = 1;
            this.Save.Text = "Save";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // BoxList
            // 
            this.BoxList.AllowDrop = true;
            this.BoxList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.BoxList.ContextMenuStrip = this.ListContextMenu;
            this.BoxList.Dock = System.Windows.Forms.DockStyle.Top;
            this.BoxList.HideSelection = false;
            this.BoxList.LargeImageList = this.ListViewImages;
            this.BoxList.Location = new System.Drawing.Point(0, 0);
            this.BoxList.Name = "BoxList";
            this.BoxList.Size = new System.Drawing.Size(270, 140);
            this.BoxList.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.BoxList.TabIndex = 2;
            this.BoxList.UseCompatibleStateImageBehavior = false;
            this.BoxList.View = System.Windows.Forms.View.Tile;
            this.BoxList.DragDrop += new System.Windows.Forms.DragEventHandler(this.BoxList_DragDrop);
            this.BoxList.DragEnter += new System.Windows.Forms.DragEventHandler(this.BoxList_DragEnter);
            // 
            // ListContextMenu
            // 
            this.ListContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeToolStripMenuItem});
            this.ListContextMenu.Name = "ListContextMenu";
            this.ListContextMenu.Size = new System.Drawing.Size(118, 26);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.removeToolStripMenuItem.Text = "Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.RemoveToolStripMenuItem_Click);
            // 
            // ListViewImages
            // 
            this.ListViewImages.ColorDepth = System.Windows.Forms.ColorDepth.Depth16Bit;
            this.ListViewImages.ImageSize = new System.Drawing.Size(32, 32);
            this.ListViewImages.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // Box
            // 
            this.AcceptButton = this.Save;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.CancelButton = this.Cancel;
            this.ClientSize = new System.Drawing.Size(270, 190);
            this.ControlBox = false;
            this.Controls.Add(this.BoxList);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.Cancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(280, 600);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(280, 200);
            this.Name = "Box";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.TopMost = true;
            this.Shown += new System.EventHandler(this.Box_Shown);
            this.ListContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.ListView BoxList;
        private System.Windows.Forms.ImageList ListViewImages;
        private System.Windows.Forms.ContextMenuStrip ListContextMenu;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
    }
}

