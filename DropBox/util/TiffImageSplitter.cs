﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;

namespace Coop.DropBox.util
{
    class TiffImageSplitter
    {
        // Retrive PageCount of a multi-page tiff image
        public int getPageCount(String fileName)
        {
            int pageCount = -1;
            try
            {
                Image img = Bitmap.FromFile(fileName);
                pageCount = img.GetFrameCount(FrameDimension.Page);
                img.Dispose();

            }
            catch (Exception ex)
            {
                pageCount = 0;
            }
            return pageCount;
        }

        public int getPageCount(Image img)
        {
            int pageCount = -1;
            try
            {
                pageCount = img.GetFrameCount(FrameDimension.Page);
            }
            catch (Exception ex)
            {
                pageCount = 0;
            }
            return pageCount;
        }

        // Retrive a specific Page from a multi-page tiff image
        public Image getTiffImage(String sourceFile, int pageNumber)
        {
            Image returnImage = null;

            try
            {
                Image sourceIamge = Bitmap.FromFile(sourceFile);
                returnImage = getTiffImage(sourceIamge, pageNumber);
                sourceIamge.Dispose();
            }
            catch (Exception ex)
            {
                returnImage = null;
            }

            //       String splittedImageSavePath = "X:\\CJT\\CJT-Docs\\CJT-Images\\result001.tif";
            //       returnImage.Save(splittedImageSavePath);

            return returnImage;
        }

        public Image getTiffImage(Image sourceImage, int pageNumber)
        {
            MemoryStream ms = null;
            Image returnImage = null;

            try
            {
                ms = new MemoryStream();
                Guid objGuid = sourceImage.FrameDimensionsList[0];
                FrameDimension objDimension = new FrameDimension(objGuid);
                sourceImage.SelectActiveFrame(objDimension, pageNumber);
                sourceImage.Save(ms, ImageFormat.Tiff);
                returnImage = Image.FromStream(ms);
            }
            catch (Exception ex)
            {
                returnImage = null;
            }
            return returnImage;
        }

        public Image AdjustContrast(Image original, float contrast)
        {
            var adjusted = new Bitmap(original.Width, original.Height);
            var g = Graphics.FromImage(adjusted);
            var attributes = new ImageAttributes();

            // create matrix that will brighten and contrast the image
            var matrix = new ColorMatrix(new float[][] {
                    new float[] {contrast, 0f, 0f, 0f, 0f}, // scale red
                    new float[] {0f, contrast, 0f, 0f, 0f}, // scale green
                    new float[] {0f, 0f, contrast, 0f, 0f}, // scale blue
                    new float[] {0f, 0f, 0f, 1f, 0f},       // don't scale alpha
                    new float[] {0.001f, 0.001f, 0.001f, 0f, 1f} // minor shift to all colors so none start at 0f, which can cause arithmatic overflows
                    });

            attributes.ClearColorMatrix();
            attributes.SetColorMatrix(matrix);

            g.DrawImage(
                original,
                new Rectangle(0, 0, original.Width, original.Height),
                0, 0,
                original.Width, original.Height,
                GraphicsUnit.Pixel,
                attributes
                );

            g.Dispose();
            attributes.Dispose();

            return adjusted;
        }
    }
}
