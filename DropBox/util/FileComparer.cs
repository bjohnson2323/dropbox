﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Coop.DropBox.util
{
    static class FileComparer
    {
        public static int Compare(FileInfo a, FileInfo b)
        {

            var byName = String.Compare(a.Name, b.Name);

            if (byName == 0)
            {

                if (a.Length > b.Length)
                {
                    return -1;
                }
                if (a.Length < b.Length)
                {
                    return 1;
                }
                return 0;
            }
            return byName;
        }

        public static bool Contains(this List<FileInfo> files, FileInfo match)
        {
            return files.Any(file => Compare(file, match) == 0);
        }
    }
}
