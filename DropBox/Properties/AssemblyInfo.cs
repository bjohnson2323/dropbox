﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("DropBox")]
[assembly: AssemblyDescription("Monitors for request files from CADOL system")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Cooperative Plating")]
[assembly: AssemblyProduct("DropBox")]
[assembly: AssemblyCopyright("Copyright © Cooperative Plating 2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("a1c4683b-f736-4a66-8dba-d41fec9d42e9")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.2.4")]
[assembly: AssemblyFileVersion("1.2.4")]
