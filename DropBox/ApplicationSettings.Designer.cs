﻿namespace Coop.DropBox
{
    partial class ApplicationSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ApplicationSettings));
            this.FindFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.OK = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.Apply = new System.Windows.Forms.Button();
            this.RequestFolderPath = new System.Windows.Forms.TextBox();
            this.BrowseForRequestFolder = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.BaseSavePath = new System.Windows.Forms.TextBox();
            this.BrowseForBaseSave = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.DPI_field = new System.Windows.Forms.NumericUpDown();
            this.ImageType_Field = new System.Windows.Forms.ComboBox();
            this.DeviceName = new System.Windows.Forms.TextBox();
            this.BrowseDevice = new System.Windows.Forms.Button();
            this.ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DPI_field)).BeginInit();
            this.SuspendLayout();
            // 
            // OK
            // 
            this.OK.Location = new System.Drawing.Point(201, 261);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(75, 23);
            this.OK.TabIndex = 8;
            this.OK.Text = "OK";
            this.OK.UseVisualStyleBackColor = true;
            this.OK.Click += new System.EventHandler(this.Ok_Click);
            // 
            // Cancel
            // 
            this.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel.Location = new System.Drawing.Point(362, 261);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(75, 23);
            this.Cancel.TabIndex = 9;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // Apply
            // 
            this.Apply.Location = new System.Drawing.Point(282, 261);
            this.Apply.Name = "Apply";
            this.Apply.Size = new System.Drawing.Size(75, 23);
            this.Apply.TabIndex = 11;
            this.Apply.Text = "Apply";
            this.Apply.UseVisualStyleBackColor = true;
            this.Apply.Click += new System.EventHandler(this.Apply_Click);
            // 
            // RequestFolderPath
            // 
            this.RequestFolderPath.Location = new System.Drawing.Point(6, 42);
            this.RequestFolderPath.Name = "RequestFolderPath";
            this.RequestFolderPath.ReadOnly = true;
            this.RequestFolderPath.Size = new System.Drawing.Size(340, 20);
            this.RequestFolderPath.TabIndex = 8;
            // 
            // BrowseForRequestFolder
            // 
            this.BrowseForRequestFolder.Location = new System.Drawing.Point(350, 42);
            this.BrowseForRequestFolder.Name = "BrowseForRequestFolder";
            this.BrowseForRequestFolder.Size = new System.Drawing.Size(75, 23);
            this.BrowseForRequestFolder.TabIndex = 9;
            this.BrowseForRequestFolder.Text = "Browse";
            this.BrowseForRequestFolder.UseVisualStyleBackColor = true;
            this.BrowseForRequestFolder.Click += new System.EventHandler(this.BrowseForRequestFolder_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Request Folder Path";
            // 
            // BaseSavePath
            // 
            this.BaseSavePath.Location = new System.Drawing.Point(6, 95);
            this.BaseSavePath.Name = "BaseSavePath";
            this.BaseSavePath.ReadOnly = true;
            this.BaseSavePath.Size = new System.Drawing.Size(340, 20);
            this.BaseSavePath.TabIndex = 11;
            // 
            // BrowseForBaseSave
            // 
            this.BrowseForBaseSave.Location = new System.Drawing.Point(350, 95);
            this.BrowseForBaseSave.Name = "BrowseForBaseSave";
            this.BrowseForBaseSave.Size = new System.Drawing.Size(75, 24);
            this.BrowseForBaseSave.TabIndex = 12;
            this.BrowseForBaseSave.Text = "Browse";
            this.BrowseForBaseSave.UseVisualStyleBackColor = true;
            this.BrowseForBaseSave.Click += new System.EventHandler(this.BrowseForBaseSave_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Base Save Path";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Scanner";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.DPI_field);
            this.groupBox1.Controls.Add(this.ImageType_Field);
            this.groupBox1.Controls.Add(this.DeviceName);
            this.groupBox1.Controls.Add(this.BrowseDevice);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.BrowseForBaseSave);
            this.groupBox1.Controls.Add(this.BaseSavePath);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.BrowseForRequestFolder);
            this.groupBox1.Controls.Add(this.RequestFolderPath);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(434, 243);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(232, 187);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Image Intent";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(215, 213);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Resolution (DPI)";
            // 
            // DPI_field
            // 
            this.DPI_field.Location = new System.Drawing.Point(305, 211);
            this.DPI_field.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.DPI_field.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.DPI_field.Name = "DPI_field";
            this.DPI_field.Size = new System.Drawing.Size(120, 20);
            this.DPI_field.TabIndex = 23;
            this.ToolTip.SetToolTip(this.DPI_field, "A higher DPI will result in better scan quality, larger file size, and longer sca" +
        "n duration.");
            this.DPI_field.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // ImageType_Field
            // 
            this.ImageType_Field.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ImageType_Field.FormattingEnabled = true;
            this.ImageType_Field.Items.AddRange(new object[] {
            "Grayscale",
            "Color"});
            this.ImageType_Field.Location = new System.Drawing.Point(304, 184);
            this.ImageType_Field.Name = "ImageType_Field";
            this.ImageType_Field.Size = new System.Drawing.Size(121, 21);
            this.ImageType_Field.TabIndex = 21;
            this.ToolTip.SetToolTip(this.ImageType_Field, "Scans with color will have larger file sizes compared to grayscale scans.");
            // 
            // DeviceName
            // 
            this.DeviceName.Location = new System.Drawing.Point(6, 148);
            this.DeviceName.Name = "DeviceName";
            this.DeviceName.ReadOnly = true;
            this.DeviceName.Size = new System.Drawing.Size(340, 20);
            this.DeviceName.TabIndex = 19;
            // 
            // BrowseDevice
            // 
            this.BrowseDevice.Location = new System.Drawing.Point(350, 148);
            this.BrowseDevice.Name = "BrowseDevice";
            this.BrowseDevice.Size = new System.Drawing.Size(75, 23);
            this.BrowseDevice.TabIndex = 18;
            this.BrowseDevice.Text = "Browse";
            this.BrowseDevice.UseVisualStyleBackColor = true;
            this.BrowseDevice.Click += new System.EventHandler(this.BrowseDevice_Click);
            // 
            // ApplicationSettings
            // 
            this.AcceptButton = this.OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Cancel;
            this.ClientSize = new System.Drawing.Size(458, 295);
            this.Controls.Add(this.Apply);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.OK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ApplicationSettings";
            this.ShowInTaskbar = false;
            this.Text = "Application Settings";
            this.TopMost = true;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DPI_field)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog FindFolder;
        private System.Windows.Forms.Button OK;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Button Apply;
        private System.Windows.Forms.TextBox RequestFolderPath;
        private System.Windows.Forms.Button BrowseForRequestFolder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox BaseSavePath;
        private System.Windows.Forms.Button BrowseForBaseSave;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox DeviceName;
        private System.Windows.Forms.Button BrowseDevice;
        private System.Windows.Forms.ComboBox ImageType_Field;
        private System.Windows.Forms.NumericUpDown DPI_field;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolTip ToolTip;
    }
}