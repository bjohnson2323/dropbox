﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Coop.DropBox.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var fileName = "C:\\b.tiff";

            new ProcessStartInfo(fileName) { Verb = "Open" };

            var sb = new StringBuilder();

            Assert.IsTrue(File.Exists(fileName),"Inorder to run the test, the target file must exist!");

            var startInfo = new ProcessStartInfo(fileName);
            
            try
            {
                startInfo.Verb = "Bad";
            }
            catch (Exception ex)
            {
                Debug.Assert(false,ex.Message);
                return;
            }

            var process = new Process();
            process.StartInfo = startInfo;

            try
            {
                process.Start();
            }
            catch (System.ComponentModel.Win32Exception e)
            {
                Console.WriteLine("  Win32Exception caught!");
                Console.WriteLine("  Win32 error = {0}", e.Message);
            }
            catch (System.InvalidOperationException)
            {
                // Catch this exception if the process exits quickly, 
                // and the properties are not accessible.
                Console.WriteLine("File {0} started with verb {1}", fileName, "Open");
            }
        }
    }
}
